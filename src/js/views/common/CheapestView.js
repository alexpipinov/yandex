define(function(require) {
	'use strict';

	var $ = require('jquery'),
		Backbone = require('backbone');

	//Modules
	var HeightController = require('modules/common/HeightController');

	return Backbone.View.extend({
		'el' : '#cheapests',

		initialize: function() {
			this.cheapest = this.$('.js-cheapest-box');
			this.heightController = new HeightController(this.cheapest, 1.1, 0.8);
		},

		destroy: function() {
			this.heightController.destroy();
			this.heightController = null;
		}
	});
});