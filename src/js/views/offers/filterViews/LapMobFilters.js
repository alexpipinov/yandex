define(function(require) {
	'use strict';
	var $ = require('jquery'),
		_ = require('underscore'),
		Backbone = require('backbone'),
		app = require('app'),
		FiltersView = require('views/offers/filterViews/FiltersView');

	var LapMobFilters =  FiltersView.extend({
		el: '#lapMobFilters',

		initialize: function() {
			LapMobFilters.__super__.initialize.call(this);

			var self = this;
			require(['modules/offersPage/mobileFiltersStateController'], function(filtersStateController) {
				self.stateController = filtersStateController;

				self._initFiltersBtn();
			});
		},

		/**
		 * @description Инициализируем кнопку открывающую панель фильтров
		 * @private
		 */
		_initFiltersBtn: function() {
			this.stateController.init();
		},

		/**
		 * @description Дестроим кнопку открывающую панель фильтров
		 * @private
		 */
		_destroyFiltersBtn: function() {
			this.stateController.destroy();
		},

		render: function() {
			return this;
		},

		destroy: function() {
			this._destroyFiltersBtn();
		}
	});

	return LapMobFilters;
});