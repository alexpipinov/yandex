define(function(require) {
	'use strict';
	var $ = require('jquery'),
		_ = require('underscore'),
		Backbone = require('backbone'),
		iCheck = require('iCheck');

	return Backbone.View.extend({
		_initIcheck: function() {
			this.icheckInstance = this.inputs.iCheck({
				checkboxClass: 'icheckbox_minimal',
				radioClass: 'iradio_minimal'
			});
		},

		initialize: function() {
			this.inputs = this.$('.js-icheck');
			this._initIcheck();

			this.render();
		},

		destroy: function() {
			this.icheckInstance.iCheck('destroy');
			this.icheckInstance = null;

			return this;
		}
	});
});