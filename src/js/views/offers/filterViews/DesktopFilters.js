define(function(require) {
	'use strict';
	var $ = require('jquery'),
		_ = require('underscore'),
		Backbone = require('backbone'),
		app = require('app'),
		FiltersView = require('views/offers/filterViews/FiltersView');

	var DesktopFilters =  FiltersView.extend({
		el: '#desktopFiltersWrapper',

		//TODO: сделать так:
		// чтобы отъезд наверх был всегда одинаковым(сейчас top при разной интенсивности скролла разный)
		// при отъезде наверх из absolute позиции убрать скачок при переходе к fixed (примерно на 20px скачет);

		_positionController: function(e) {
			var curScrollPos = app.dom.$window.scrollTop();
			var boxBottomPos = this.el.getBoundingClientRect().bottom;
			var boxPosTop = parseInt(window.getComputedStyle(this.el).top);

			//Координаты
			var absStickyPos = this.scrollHeight - this.el.offsetHeight - this.footerHeight;
			//Координаты для прилипания низа блока
			var fixStickyBottomPos = boxPosTop  - (curScrollPos - this.initialScrollPos);

			//Условия
			//Если скролл опустился ниже футера
			var scrollYPosMoreFooterTopPos =  this.scrollHeight - (curScrollPos + this.screenHeight)  < this.footerHeight;

				//Если скролл отрицательный, ничего не делаем
				if (curScrollPos < 0 ) return;

				//Определяем направление скролла (Вниз / Вверх)
				if (curScrollPos > this.initialScrollPos) {
					if (boxBottomPos >= this.screenHeight && !scrollYPosMoreFooterTopPos) {
						this._isFixedPosition(fixStickyBottomPos);
						this.lastPosTop = boxPosTop;
					}else if (scrollYPosMoreFooterTopPos) {
						this._isAbsPosition(absStickyPos);
					}
				}else {
					if (scrollYPosMoreFooterTopPos) {
						this._isAbsPosition(absStickyPos);
					}else {
						if (this.lastPosTop) {
							this._isFixedPosition(this.lastPosTop);
							this.lastPosTop = null;
						}else {
							if (boxPosTop < this.boxInitialTop) {
								this._isFixedPosition(fixStickyBottomPos);
							}else {
								//Прилипает верхом, top == начальной позиции
								this._isFixedPosition(this.boxInitialTop);
							}
						}
					}
				}

				this.initialScrollPos = curScrollPos;
		},

		_isFixedPosition: function(posTop) {
			this.el.style.position = 'fixed';
			this.el.style.top = posTop  + 'px';
		},

		_isAbsPosition: function(posTop) {
			this.el.style.top = posTop + 'px';
			this.el.style.position = 'absolute';
		},

		/**
		 * Определяет базовые точки отсчета
		 * @private
		 */
		_defineMainData: function() {
			this.scrollHeight = document.documentElement.scrollHeight;
			this.screenHeight = document.documentElement.clientHeight;

			this.footerHeight = document.querySelector('.b-footer').offsetHeight;

			//Чтобы не было багов при отстутсвии прокрутки
			this.scrollHeight = Math.max(this.scrollHeight, this.screenHeight);

			this.offersContainerHeight = document.getElementById('offers').offsetHeight;
		},

		initialize: function() {
			DesktopFilters.__super__.initialize.apply(this);

			//Выставляем принудительно в начало документа
			window.scrollTo(0, 0);

			this.boxInitialTop = parseInt(window.getComputedStyle(this.el).top);
			this.initialScrollPos = app.dom.$document.scrollTop();

			this._defineMainData();

			this._positionController();
			this._subscribers();
		},

		_subscribers: function() {
			app.dom.$window.on('scroll.stickyDesktopFilters', this._positionController.bind(this));
			app.dom.$window.on('resize.stickyDesktopFilters', this._defineMainData.bind(this));
		},

		render: function() {
			return this;
		},

		destroy: function() {
			DesktopFilters.__super__.destroy.apply(this);
			app.dom.$window.off('scroll.stickyDesktopFilters');
			app.dom.$window.off('resize.stickyDesktopFilters');
		}

	});

	return DesktopFilters;
});