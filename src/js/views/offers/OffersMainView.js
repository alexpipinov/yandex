define(function(require) {
	'use strict';
	var $ = require('jquery'),
		_ = require('underscore'),
		Backbone = require('backbone'),
		app = require('app'),
		selecter = require('selecter');

	//Modules
	var favoritesController = require('modules/common/favoritesController');

	//Views
	var offersListView = require('views/offers/offersList/offersListView'),
		commonFiltersView = require('views/offers/commonFiltersView/commonFiltersView');

	return Backbone.View.extend({
		el: '#offers',

		/**
		 * @description Инициализирует десктопную версию фильтров
		 * @private
		 */
		_initDesktopFilters: function() {
			var self = this;
			require(["views/offers/filterViews/DesktopFilters"], function (DesktopFilters) {
				self.desktopFilters = new DesktopFilters();
			});

		},

		/**
		 * @description Инициализирует десктопную версию селекта категорий
		 * @private
		 */
		_initDesktopCategorySelect : function() {
			var self = this;
			require(["views/offers/deskCategorySelectView/deskCategorySelectView"], function (DeskCategorySelectView) {
				self.deskCategorySelect = new DeskCategorySelectView();
			});
		},

		/**
		 * @description Инциализирует фильтры для планшетной и мобильной версий
		 * @private
		 */
		_initLapMobFilters: function() {
			var self = this;
			require(["views/offers/filterViews/LapMobFilters"], function (LapMobFilters) {
				self.lapMobileFilters = new LapMobFilters();
			});

		},

		/**
		 * @description Инициализирует селект сортировки
		 * @private
		 */
		_initSortSelect : function() {
			this.sortSelect.selecter({
				label: "Сортировать",
				customClass: 'b-select b-select_type_button'
			});

		},


		/**
		 * @description Инициализирует нужные тип фильтров для текущего шаблона
		 * @param e
		 * @private
		 */
		_filterStateController: function(e) {
			var layoutType = e.name;

			if (layoutType === 'desktop') {
				if (this.desktopFilters) return;

				//Если активны мобильные и планшетные фильтры, дестроим
				if (this.lapMobileFilters) {
					this._destroyLapMobileFilters();
				}

				if (!this.desktopFilters) {
					this._initDesktopFilters()
				}

			}else {
				if (this.lapMobileFilters) return;

				//Если активны мобильные и планшетные фильтры, дестроим
				if (this.desktopFilters) {
					this._destroyDesktopFilters();
				}

				if (this.deskCategorySelect) {
					this._destroyCategorySelect();
				}

				this._initLapMobFilters();
			}
		},

		initialize: function() {
			if (app.layoutType === 'desktop') {
				this._initDesktopFilters();
			}else {
				this._initLapMobFilters();
			}

			//Вьюха контроллер для разных типов предложений
			this.offersList = new offersListView();

			this._initDesktopCategorySelect();

			//Селект сортировки
			this.sortSelect = this.$('.js-sort-select');
			this._initSortSelect();

			//Инициализируем модуль, отвечающий за добавление в избранные
			favoritesController();

			//Инициализируем контроллер для кнопок переключения (Все, популярные, избранные)
			this.commonFilters = new commonFiltersView();

			Backbone.on('desktopLayout', this._filterStateController, this);
			Backbone.on('laptopLayout', this._filterStateController, this);
			Backbone.on('mobileLayout', this._filterStateController, this);

			this.render();
		},

		render: function() {
			return this;
		},


		/**
		 * @description Дестроим селект десктопных категорий
		 * @private
		 */
		_destroyCategorySelect: function() {
			this.deskCategorySelect.destroy();
			this.deskCategorySelect = null;
		},

		/**
		 * @description Дестроит селект сортировки
		 * @private
		 */
		_destroySortSelect: function() {
			this.sortSelect.selecter('destroy');
			this.sortSelect = null;
		},
		/**
		 * @description Дестроит десктопную версию фильтров
		 * @private
		 */
		_destroyDesktopFilters: function() {
			this.desktopFilters.destroy();
			this.desktopFilters = null;
		},

		/**
		 * @description Дестроит фильтры для планшета и мобильных
		 * @private
		 */
		_destroyLapMobileFilters: function() {
			this.lapMobileFilters.destroy();
			this.lapMobileFilters = null;
		},

		/**
		 * @description Общий дестрой для всех объектов вьюхи
		 */
		destroy: function() {
			if (this.sortSelect) {
				this.sortSelect.selecter('destroy');
			}
			if (this.deskCategorySelect) {
				this._destroySortSelect();
			}
			if (this.lapMobileFilters) {
				this._destroyLapMobileFilters();
			}
			if (this.desktopFilters) {
				this._destroyDesktopFilters();
			}

			this.commonFilters.destroy().remove();
			this.offersList.destroy().remove();
		}
	});
});