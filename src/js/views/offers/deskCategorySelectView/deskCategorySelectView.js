define(function(require) {
	'use strict';

	var $ = require('jquery'),
		Backbone = require('backbone'),
		ddslick = require('ddslick');

	return Backbone.View.extend({
		el: "#category_select",

		initialize: function() {
			this.$el.ddslick({
				onSelected: function(selectedData) {
					//console.log(selectedData);
				}
			});
		},

		destroy: function() {
			this.$el.ddslick('destroy');
		}
	});
});