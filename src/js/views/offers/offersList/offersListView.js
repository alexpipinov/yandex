define(function(require) {
	'use strict';

	var $ = require('jquery'),
		Backbone = require('backbone'),
		_ = require('underscore'),
		app = require('app');

	//Modules
	var HeightController = require('modules/common/HeightController');

	//Views
	var allView = require('views/offers/offersList/all/allOffersView'),
		cheapestView = require('views/offers/offersList/cheapest/cheapestOfferView'),
		advSlideView = require('views/offers/offersList/advSlide/advSlideView');

	return Backbone.View.extend({
		el: '#offersList',

		currentViews : [],

		/**
		 * @description Сравнивает текущий тип списка предложений с запрашиваемым, если совпадают, то игнорируем переключалку
		 * @param e
		 * @private
		 */
		_compareWithCurrentType: function(e) {
			this.targetType = e.name;

			if (this.targetType !== this.currentType) {
				this.currentType = this.targetType;

				//Сначала очистим старый контроллер
				if (this.heightController) {
					this.heightController.destroy();
					this.heightController = null;
				}

				this._getData();
			}
		},

		_getData: function() {
			var self = this;

			//TODO: тестовый вариант получения данных
			$.ajax({
				url: "templates/pages/offers-page/offers.json",
				dataType: "json",
				success: function(data) {
					//Получили данные, начинаем формировать вывод
					self._generateData(data);
				}
			});
		},

		/**
		 * @description Генерируем верстку
		 * @param data
		 * @private
		 */
		_generateData: function(data) {
			var self = this,
				advBannerPosition = this.targetType === 'cheapest' ? 6 : 2;

				this._destroyCurrentViews();
				this.$el.empty();

				_.each(data, function(offerData, key) {
					if (key === advBannerPosition) {
						self.advSlide = new advSlideView();
						self.currentViews.push(self.advSlide);

						self.$el.append(self.advSlide.render().el);
					}

					//TODO:этот вариант переделать
					switch (self.targetType) {
						case "cheapest" :
							self._addOneOffer(cheapestView, offerData);
							break;
						case "all":
							self._addOneOffer(allView, offerData);
							break;
						case "favorites":
							console.log('favorites');
							break;
						default:
							break;
					}
				});

				if (this.targetType === 'cheapest') {
					this._setHeight();
				}else {
					if (this.heightController) {
						this.heightController.destroy();
					}
				}
		},

		/**
		 * @description Добавляет по одному предложению
		 * @param targetView
		 * @param offerData
		 * @private
		 */
		_addOneOffer: function(targetView, offerData) {
			var view = new targetView({offer: offerData});
				this.$el.append(view.render().el);
				this.currentViews.push(view);
		},

		_addAllOffers: function() {

		},

		_setHeight: function() {
			var $cheapestBox = $('.js-cheapest-box');
			this.heightController = new HeightController($cheapestBox, 1.1, 0.8);
		},


		initialize: function() {
			//TODO: Временное решение, это значение нужно присваивать динамически в зависимости от урла или другого параметра,
			this.currentType = 'all';

			//Если переход аяксовый, тогда генерим верстку, иначе приходит с сервера
			if (!app.loadCounter) {

			}

			this._subscribers();
		},

		_subscribers: function() {
			Backbone.on('switchOffersType', this._compareWithCurrentType, this);
		},

		render: function() {
			return this;
		},


		/**
		 * @description Дестроит вьюхи при переключении на другой тип предложений
		 * @private
		 */
		_destroyCurrentViews: function() {
			_.invoke(this.currentViews, 'remove');
			this.currentViews.length = 0;
		},

		/**
		 * @description Отвязывает все события, дестроит все sub-views
		 */
		destroy: function() {
			Backbone.on('switchOffersType', null, this);
			if (this.heightController) {
				this.heightController.destroy();
				this.heightController = null;
			}
			this._destroyCurrentViews();
		}
	});
});