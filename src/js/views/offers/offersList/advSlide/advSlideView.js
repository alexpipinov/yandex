define(function(require) {
	'use strict';

	var $ = require('jquery'),
		Backbone = require('backbone'),
		twig = require('twig').twig;

	return Backbone.View.extend({
		tagName: 'article',
		className: 'b-slide b-single-slide_type_inner',

		template: twig({
			id: 'advSlide',
			href: 'templates/pages/offers-page/b-adv-slide/b-adv-slide.twig',
			async: true,
			allowInlineIncludes: true
		}),

		initialize: function() {

		},

		render: function() {
			this.$el.html($(twig({ref: 'advSlide'}).render()));
			return this;
		},

		destroy: function() {

		}
	});

});