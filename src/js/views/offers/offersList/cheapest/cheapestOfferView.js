define(function(require) {
	'use strict';

	var $ = require('jquery'),
		Backbone = require('backbone'),
		twig = require('twig').twig;

	return Backbone.View.extend({
		tagName: 'article',
		className: 'b-cheapest js-cheapest-box',

		template: twig({
			id: 'popularOffer',
			href: 'templates/pages/offers-page/b-offer-cheapest/cheapestOffer.twig',
			async: true
		}),

		initialize: function(data) {
			this.data = data;
		},

		render: function() {
			this.$el.html($(twig({ref: 'popularOffer'}).render(this.data)));
			return this;
		},

		destroy: function() {
			return this;
		}
	});
});