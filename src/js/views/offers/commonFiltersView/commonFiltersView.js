define(function(require) {
	'use strict';

	var $ = require('jquery'),
		Backbone = require('backbone');

	return Backbone.View.extend({
		el: '#commonFilters',

		_allOffersTrigger: function(e) {
			e.preventDefault();
			Backbone.trigger('switchOffersType', {name: 'all'});
		},
		_cheapestTrigger: function(e) {
			e.preventDefault();
			Backbone.trigger('switchOffersType', {name: 'cheapest'});
		},
		_favoritesTrigger: function(e) {
			e.preventDefault();
			Backbone.trigger('switchOffersType', {name: 'favorites'});
		},

		_switchActiveClass: function(e) {
			this.$('.js-filters-common-btn').removeClass('_is-active');
			$(e.currentTarget).addClass('_is-active');
		},

		initialize: function() {
			this.allBtn			= this.$('.js-filters-common-btn');
			this.allOffersBtn 	= this.$('.js-offers-filters-all-btn');
			this.cheapestBtn 	= this.$('.js-cheapest-btn');
			this.favoritesBtn 	= this.$('.js-offers-filters-favorites-btn');

			this._subscribers();
		},

		_subscribers: function() {
			this.allOffersBtn.on('click', this._allOffersTrigger);
			this.cheapestBtn.on('click', this._cheapestTrigger);
			this.favoritesBtn.on('click', this._favoritesTrigger);
			this.allBtn.on('click', this._switchActiveClass.bind(this));
		},

		destroy: function() {
			this.allOffersBtn.off();
			this.cheapestBtn.off();
			this.favoritesBtn.off();

			return this;
		}
	});
});