define(function(require) {
	'use strict';
	var $ = require('jquery'),
		Backbone = require('backbone'),
		app = require('app');


	return Backbone.View.extend({
		el: '#notifications',

		initialize: function() {
			this.render();
		},

		render: function() {
			return this;
		},

		/**
		 * @description Общий дестрой для всех объектов вьюхи
		 */
		destroy: function() {

		}
	});
});