define(function(require) {
	'use strict';

	var $ = require('jquery'),
		Bakbone = require('backbone'),
		app	= require('app');

	return Backbone.View.extend({
		el: '#searchBox',

		_openSearchBox: function() {
			app.dom.$body.addClass('i-searchBoxIsOpened');
			this.$el.addClass('_is-active');
		},

		_closeSearchBox: function() {
			app.dom.$body.removeClass('i-searchBoxIsOpened');
			this.$el.removeClass('_is-active');
		},

		_closeTrigger: function() {
			Backbone.trigger('desktopSearchCloseBtnIsClicked', {name: 'desktopSearchClose'} );
		},

		subscribers: function() {
			Backbone.on('desktopSearchOpen', this._openSearchBox, this);
			Backbone.on('desktopSearchClose', this._closeSearchBox, this);

			this.closeBtn.on('click', this._closeTrigger.bind(this));
		},

		initialize: function() {
			this.closeBtn = this.$('#desktopSearchClose');
			this.subscribers();
		},

		destroy: function() {
			Backbone.off('desktopSearchOpen');
			Backbone.off('desktopSearchClose');
		}
	});
});