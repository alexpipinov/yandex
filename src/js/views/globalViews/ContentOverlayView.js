define(function(require) {
	'use strict';

	var $ = require('jquery'),
		Bakbone = require('backbone'),
		app = require('app'),
		helpers = require('helpers');

	return Backbone.View.extend({
		el: '#contentOverlay',

		isClicked: function(e) {
			e.preventDefault();
			Backbone.trigger('contentOverlayIsClicked', {name: 'contentOverlay'});
		},

		subscribers: function() {
			this.$el.on(helpers.isTouchDevice() ? 'touchstart' : 'click', this.isClicked);
		},

		initialize: function() {
			this.subscribers();
		},

		destroy: function() {
			this.$el.off();
			return true;
		}
	});
});