define(function(require) {
	'use strict';

	var $ = require('jquery'),
		Bakbone = require('backbone'),
		app	= require('app'),
		helpers	= require('helpers'),
		scrollbar = require('scrollbar');

	return Backbone.View.extend({
		el: '#nav',

		customScrollInit: function() {
			this.$el.mCustomScrollbar({
				scrollInertia: 3
			})
		},

		openNav: function() {
			app.dom.$body.addClass('i-menu-is-opened');
		},

		closeNav: function() {
			app.dom.$body.removeClass('i-menu-is-opened');
		},

		closeNavFromOverlay: function() {
			if (!app.dom.$body.hasClass('i-menu-is-opened')) return;

			this.closeNav();
		},

		initialize: function() {
			this.$navStateTriggerBtn = $('.js-nav-state-trigger-btn');

			//Если не touch устройство активируем кастомный скролл для навигации
			if(!helpers.isTouchDevice()) {
				this.customScrollInit();
			}

			this.subscribers();
		},

		//Сообщаем медиатору, что кнопка открытия меню была нажат
		navStateTrigger: function() {
			Backbone.trigger('navBtnIsClicked', {name: 'navBtn'});
		},

		subscribers: function() {
			this.$navStateTriggerBtn.on('click', this.navStateTrigger);

			Backbone.on('navOpen', this.openNav.bind(this));
			Backbone.on('navClose', this.closeNav.bind(this));

			app.dom.$document.on('contentOverlayIsClicked.nav', this.closeNavFromOverlay.bind(this));
		},

		destroy: function() {
			app.dom.$document.off('contentOverlayIsClicked.nav');
		}
	});
});