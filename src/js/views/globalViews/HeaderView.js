define(function(require) {
	'use strict';

	var $ = require('jquery'),
		Baсkbone = require('backbone'),
		app = require('app'),
		selecter = require('selecter');



	return  Backbone.View.extend({
		el: '#mainHeader',

		/**
		 * Триггерит клик по кнопке поиска
		 * @private
		 */
		_headerSearchBtnTrigger: function() {
			Backbone.trigger('headerSearchBtnClicked', {name: 'headerSearchBtn'});
		},

		/**
		 * @description Инициализирует селект категорий
		 * @private
		 */
		_initCategorySelect : function() {
			if (!this.categorySelectInstance) {
				this.categorySelectInstance = this.categorySelect.selecter({
					customClass: 'b-select b-select_type_header'
				});
			}
		},
		_destroyCategorySelect: function() {
			this.categorySelectInstance.selecter("destroy");
			this.categorySelectInstance = null;
		},


		initialize: function() {
			this.$headerSearchBtn = this.$('#headerSearchBtn');

			this.categorySelect = this.$('.js-select-category-header');

			if (app.layoutType !== 'desktop') {
				this._initCategorySelect();
			}

			this.subscribers();
		},

		subscribers: function() {
			this.$headerSearchBtn.on('click', this._headerSearchBtnTrigger);

			Backbone.on('laptopLayout mobileLayout', this._initCategorySelect, this);
			Backbone.on('desktopLayout', this._destroyCategorySelect, this);
		},

		destroy: function() {
			if (this.categorySelectInstance) {
				this._destroyCategorySelect();
			}

			Backbone.off(null, null, this);
		}
	});
});