define(function(require) {
	'use strict';

	var $ = require('jquery'),
		Bakbone = require('backbone'),
		app	= require('app'),
		helpers = require('helpers'),
		cookie = require('cookies');

	//Sub-viwes
	var CheapestView = require('views/common/CheapestView'),
		CategoryTileView = require('views/mainPageViews/CategoryTileView');

	//Modules
	var mobileHover = require('modules/common/mobileHover');


	return Backbone.View.extend({
		el: '#page',

		initialize: function() {
			var self = this;
			this.showQuickTour = (app.layoutType !== 'mobile') && this.$('#quickTour').length;

			// Если раньше пользователь не закрывал quickTour и тип шаблона позволяет его отобразить
			if ($.cookie('quickTour') !== 'closed' && this.showQuickTour) {
				require(['views/mainPageViews/QuickTourView'], function(QuickTourView) {
					self.QuickTourView = QuickTourView;
					self._showQuickTour();
				});
			}

			//Инициализируем блоки с популярными предложениями
			app.currentViews.push(new CheapestView());

			//Инициализируем блоки с категорий (Плитка)
			app.currentViews.push(new CategoryTileView());

			//Подписываемся на события
			this._subscriptions();

			if (helpers.isTouchDevice()) {
				this.mobileHovers = mobileHover();
			}
		},

		_subscriptions: function() {
			Backbone.on('showQuickTour', this._showQuickTour, this);
			Backbone.on('hideQuickTour',  this._hideQuickTour, this);
		},


		/**
		 * Инициализируем quick tour
		 * @private
		 */
		_showQuickTour: function() {
			if ($.cookie('quickTour') !== 'closed' && this.showQuickTour) {
				//Запрещаем повтороную инициализацию
				this.showQuickTour = false;

				this.quickTour = new this.QuickTourView();
			}
		},

		/**
		 * Дестроим quick tour и все сопутствующие объекты
		 * @private
		 */
		_hideQuickTour: function() {
			if (this.quickTour) {
				this.quickTour.destroy().stopListening();
				this.quickTour = null;
			}

			//Разрешаем повтороную инициализацию
			this.showQuickTour = true;
		},


		destroy: function() {
			this.quickTour.destroy().stopListening();
			this.quickTour = null;

			Backbone.off('showQuickTour');
			Backbone.off('hideQuickTour');

			if (this.mobileHovers) {
				this.mobileHovers.destroy();
			}
		}
	});
});