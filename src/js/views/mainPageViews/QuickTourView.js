define(function(require) {
	'use strict';

	var $ = require('jquery'),
		Backbone = require('backbone'),
		app = require('app'),
		cookie = require('cookies'),
		enquire = require('enquire'),
		owlCarousel = require('owl');

	//Modules
	var arrowMover = require('modules/arrowMover');

	return Backbone.View.extend({
		el: '#quickTour',

		events: {
			'click .js-quick-tour-close': '_closeTour'
		},

		/**
		 * Переключаем слайд карусели, после клика по контроллу
		 * @param $targetSlide
		 * @private
		 */
		_setTargetSlide : function($targetSlide) {
			this.$carousel.data('owlCarousel').goTo($targetSlide);

			//Перемещаем стрелку над контроллами
			arrowMover($targetSlide);
		},

		/**
		 * Устанавливаем активный контролл
		 * @param $targetControll
		 * @private
		 */
		_setTargetControll: function($targetControll) {
			$targetControll.addClass('is-active');

		},

		/**
		 * Сбрасываем все активные классы у контроллов
		 * @private
		 */
		_resetControllState: function() {
			this.$controlls.removeClass('is-active');
		},

		/**
		 * Инициализируем карусель
		 */
		initCarousel: function() {
			var self = this;

				this.$carousel.owlCarousel({
					slideSpeed: 400,
					paginationSpeed: 400,
					rewindSpeed: 400,
					singleItem: true,
					pagination: false,
					navigation: true,
					navigationText: ['<i class="b-carousel__arrow b-carousel__arrow_direction_left"></i>', '<i class="b-carousel__arrow b-carousel__arrow_direction_right"></i>'],
					mouseDrag: false,

					afterInit: function () {
						self.$controlls.on('click', function (e) {
							e.stopPropagation();
							var $this = $(this),
								$targetItem = parseInt($this.data('item'));

								if (!$this.hasClass('_is-active')) {
									//переезд к нужному слайду
									self._setTargetSlide($targetItem);
								}
						});
					},
					afterAction: function() {
						var currentItem = this.currentItem,
							$targetControll = self.$controlls.filter('[data-item='+ currentItem +']');

						    //найдем активный контролл и уберем активность
							self._resetControllState();

						    //установим активный контроллл
							self._setTargetControll($targetControll);

						    //Переезд указателя arrow
							arrowMover(currentItem);
					}
				});
		},

		/**
		 * Запускает процесс закрыти тура
		 * @private
		 */
		_closeTour : function(e) {
			e.preventDefault();
			this.destroy();

			//Ставим куку, пока она есть, quick tour не будет больше инициализироваться
			$.cookie('quickTour', 'closed');
		},

		initialize: function() {
			this.$carousel = this.$('.js-owl-carousel');
			this.$controllsBox = this.$('.js-carousel-controlls');
			this.$controlls = this.$('.js-carousel-controll');
			this.$controllsArrow = this.$('.js-arrow-top');

			this.$el.addClass('is-visible');

			//Инициализируем карусель
			this.initCarousel();

			//Перемещаем стрелку над контроллами
			arrowMover();
		},

		destroy: function() {
			this.$el.removeClass('is-visible');
			this.$controllsBox.addClass('hide');
			this.$carousel.data('owlCarousel').destroy();
			this.$controlls.off();

			return this;
		}
	});
});