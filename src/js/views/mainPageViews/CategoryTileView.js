define(function(require) {
	'use strict';

	var $ = require('jquery'),
		Backbone = require('backbone'),
		twig = require('twig').twig;

	//Modules
	var HeightController = require('modules/common/HeightController');


	var CategoryTileView = Backbone.View.extend({
		el: '.js-category-tile',

				initialize: function() {
			var self = this;

			this.heightController = new HeightController(this.$el, 0.54, 0.8);
			this.render();
		},

		render: function() {

			return this;
		},

		destroy: function() {
			this.heightController.destroy();
			return this;
		}
	});

	return CategoryTileView;
});