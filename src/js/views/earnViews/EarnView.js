define(function(require) {
	'use strict';

	var $ = require('jquery'),
		Backbone = require('backbone');

	var Accordeon = require('modules/accordeon/Accordeon');

	//Views
	var FiltersView = require('views/earnViews/filtersView/FiltersView');

	return Backbone.View.extend({
		el: '#earn',

		initialize: function() {
			this.filtersInstance = new FiltersView();
			this.accordeonInstance = new Accordeon($('.js-accordeon-box'));
			this.render();
		},

		render: function() {
			return this;
		},

		destroy: function() {
			this.accordeonInstance.destroy();
		}
	});
});