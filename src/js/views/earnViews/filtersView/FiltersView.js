define(function(require) {
	'use strict';

	var $ = require('jquery'),
		Backbone = require('backbone'),
		pickadate = require('libs/pickadate/lib/compressed/picker'),
		picker = require('libs/pickadate/lib/compressed/picker.date'),
		selecter = require('selecter');

	return Backbone.View.extend({
		el: '#earnFilters',

		/**
		 * @description Инициализирует селект сортировки
		 * @private
		 */
		_initSortSelect : function() {
			this.sortSelect.selecter({
				customClass: 'b-select b-select_type_button'
			});
		},
		/**
		 * @description Дестроит селект сортировки
		 * @private
		 */
		_destroySortSelect: function() {
			this.sortSelect.selecter('destroy');
			this.sortSelect = null;
		},


		initStartDatepicker: function() {
			var self = this;

			this.startDatepicker = this.startInput.pickadate({
				firstDay: 1,
				monthsFull: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
				weekdaysShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
				today: 'Сегодня',
				clear: 'Сбросить',
				close: 'Закрыть',
				max: true
			});

			this.startPickadate = this.startDatepicker.pickadate('picker')
		},

		initEndDatepicker: function() {
			var self = this;

			this.endDatepicker = this.endInput.pickadate({
				firstDay: 1,
				monthsFull: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
				weekdaysShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
				today: 'Сегодня',
				clear: 'Сбросить',
				close: 'Закрыть',
				max: true
			});

			this.endPickadate = this.endDatepicker.pickadate('picker');
		},

		initialize: function() {
			this.startInput = this.$('.js-datepicker-start');
			this.endInput = this.$('.js-datepicker-end');

			//Селект сортировки
			this.sortSelect = this.$('.js-sort-select');
			this._initSortSelect();

			this.initStartDatepicker();
			this.initEndDatepicker();

			this.checkCurrentDates();

			this.subscribers();
			this.render();
		},


		/**
		 * Если в календарях выбран диапазон, то disable для дат, выходящих за пределы его
		 */
		checkCurrentDates: function() {
			// Check if there’s a “from” or “to” date to start with.
			if ( this.endPickadate.get('value') ) {
				this.endPickadate.set('min', this.startPickadate.get('select'))
			}
			if ( this.startPickadate.get('value') ) {
				this.startPickadate.set('max', this.endPickadate.get('select'))
			}
		},

		subscribers: function() {
			var self = this;

			this.startPickadate.on('set', function(event) {
				if ( event.select ) {
					self.endPickadate.set('min', self.startPickadate.get('select'))
				}
				else if ( 'clear' in event ) {
					self.endPickadate.set('min', false)
				}
			});

			this.endPickadate.on('set', function(event) {
				if ( event.select ) {
					self.startPickadate.set('max', self.endPickadate.get('select'))
				}
				else if ( 'clear' in event ) {
					self.startPickadate.set('max', false)
				}
			});
		},

		render: function() {
			return this;
		},

		destroy: function() {
			if (this.sortSelect) {
				this.sortSelect.selecter('destroy');
			}
		}
	});
});