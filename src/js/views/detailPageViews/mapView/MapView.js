define(function(require) {
	'use strict';

	var $ = require('jquery'),
		Baсkbone = require('backbone'),
		ymaps = require('ymaps');

	return Backbone.View.extend({
		el: '#detailPageMap',

		initialize: function() {
			var self = this;

			this.$createRouteBtn = $('.js-create-route');

			//Дожидаемся готовности и инициализируем
			ymaps.ready(self._initMap.bind(self));

			this._subscribers();
		},

		_subscribers: function() {
			this.$createRouteBtn.on('click', this._getCurrentPosition.bind(this));
		},

		/**
		 * @description Получает текущую геопозицию
		 * @param e
		 * @private
		 */
		_getCurrentPosition: function(e) {
			e.preventDefault();
			var self = this;

			var geolocation = ymaps.geolocation;

			geolocation.get({
				provider: 'browser',
				mapStateAutoApply: true
			}).then(function (result) {
				result.geoObjects.options.set('preset', 'islands#blueCircleIcon');

				//Создаем маршрут передав наши координаты
				self._createRoute(result.geoObjects.position[0], result.geoObjects.position[1]);
			});
		},

		/**
		 * Создание мультимаршрута.
		 * @param {Object} model Модель мультимаршрута. Задается объектом с полями:
		 * referencePoints - описание опорных точек мультимаршрута (обязательное поле);
		 * params - параметры мультимаршрута.
		 * @param {Object} [options] Опции маршрута.
		 * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/multiRouter.MultiRoute.xml
		 */
		_createRoute: function(one, two) {
			var multiRoute = new ymaps.multiRouter.MultiRoute({
				referencePoints: [
					[one, two], // Текущая позиция
					[55.758262, 37.594821] //Позиция соответствующая адресу заведения
				]
			}, {
				// Автоматически устанавливать границы карты так,
				// чтобы маршрут был виден целиком.
				boundsAutoApply: true,
				zoomMargin: 30
			});
			this.map.geoObjects.add(multiRoute);
		},

		_initMap: function() {
			this.map =new ymaps.Map('detailPageMap', {
				center: [55.758262,37.594821],
				zoom: 17,
				controls: ['fullscreenControl']
			});

			this._createPlacemark();
			this._createZoomControls();

			//Отключаем некоторое поведение карты
			this.map.behaviors.disable([]);
		},

		/**
		 * @description Создает кастомную метку
		 * @private
		 */
		_createPlacemark: function() {
			this.myPlacemark = new ymaps.Placemark(this.map.getCenter(), {
			}, {
				// Опции.
				// Необходимо указать данный тип макета.
				iconLayout: 'default#image',
				// Своё изображение иконки метки.
				iconImageHref: 'images/maps/singlePointerDot.png',
				// Размеры метки.
				iconImageSize: [122, 122],
				// Смещение левого верхнего угла иконки относительно
				// её "ножки" (точки привязки).
				iconImageOffset: [-61, -61]
			});

			this.map.geoObjects.add(this.myPlacemark);
		},

		/**
		 * @description Стилизует кнопки управления масштабом
		 * @private
		 */
		_createZoomControls: function() {
			var ZoomLayout = ymaps.templateLayoutFactory.createClass("<div>" +
			"<div id='zoom-in' class='b-map-controll__zoom'><i class='b-map-controll__zoom_plus'>+</i></div><br>" +
			"<div id='zoom-out' class='b-map-controll__zoom'><i class='b-map-controll__zoom_minus'>–</i></div>" +
			"</div>", {

				// Переопределяем методы макета, чтобы выполнять дополнительные действия
				// при построении и очистке макета.
				build: function () {
					// Вызываем родительский метод build.
					ZoomLayout.superclass.build.call(this);

					// Привязываем функции-обработчики к контексту и сохраняем ссылки
					// на них, чтобы потом отписаться от событий.
					this.zoomInCallback = ymaps.util.bind(this.zoomIn, this);
					this.zoomOutCallback = ymaps.util.bind(this.zoomOut, this);

					// Начинаем слушать клики на кнопках макета.
					$('#zoom-in').bind('click', this.zoomInCallback);
					$('#zoom-out').bind('click', this.zoomOutCallback);
				},

				clear: function () {
					// Снимаем обработчики кликов.
					$('#zoom-in').unbind('click', this.zoomInCallback);
					$('#zoom-out').unbind('click', this.zoomOutCallback);

					// Вызываем родительский метод clear.
					ZoomLayout.superclass.clear.call(this);
				},

				zoomIn: function () {
					var map = this.getData().control.getMap();
					// Генерируем событие, в ответ на которое
					// элемент управления изменит коэффициент масштабирования карты.
					this.events.fire('zoomchange', {
						oldZoom: map.getZoom(),
						newZoom: map.getZoom() + 1
					});
				},

				zoomOut: function () {
					var map = this.getData().control.getMap();
					this.events.fire('zoomchange', {
						oldZoom: map.getZoom(),
						newZoom: map.getZoom() - 1
					});
				}
			});

			this.zoomControl = new ymaps.control.ZoomControl({ options: { layout: ZoomLayout } });
			this._addZoomControls();
		},

		/**
		 * @description Добавляет кастомные кнопки управления масштабом
		 * @private
		 */
		_addZoomControls: function() {
			this.map.controls.add(this.zoomControl);
		},

		destroy: function() {
			this.map.destroy();
			return this;
		}
	});
});