define(function(require) {
	'use strict';

	var $ = require('jquery'),
		Backbone = require('backbone'),
		_ = require('underscore'),
		app = require('app');

	var detailHeaderController = require('modules/detailPage/detailHeaderController');

	return Backbone.View.extend({
		el: '#stickyNav',

		//Массив с блоками, к которым нужно отскроливать
		targetBoxes: [],

		/**
		 * @description  Возвращает начальную позицию прилипающей навигации
		 * @returns {}
		 * @private
		 */
		_setInitialStickyBoxPosY: function() {
			this.$el.css('top', this.posY);
		},

		/**
		 * @description Вычисляет начальную позицию для прилипающего блока
		 * @private
		 */
		_defineStickyBoxPosY: function() {
			return (this.relativeBox.offset().top + this.relativeBox.height());
		},

		/**
		 * @description  Собирает данные обо всех блоках
		 * @private
		 */
		_defineMainData: function() {
			this.posY =  this._defineStickyBoxPosY();
			this.headerHeight = this.header.height();
			this.detailPageHeaderHeight = this.detailPageHeader.height();
		},

		/**
		 * @description Определяет правильное смещение прилипающей меню относительно верха
		 * @private
		 */
		_defineRatio: function() {
			if (this.isFixedState) {
				return app.layoutType === 'mobileLayout' ? 120 : 150;
			}else {
				return app.layoutType === 'mobileLayout' ? 90 : 100;
			}
		},

		/**
		 * @description Определяем целевые блоки
		 * @private
		 */
		_defineTargetBoxes : function() {
			var self = this;

			this.links = this.$('.js-sticky-link');

			_.each(this.links, function(link) {
				self.targetBoxes.push($(link).data('target'));
			});
		},


		/**
		 * @description Устанавливает класс _is-active к ссылке на активный блок
		 * @private
		 */
		_setActiveClassToLink: function() {
			var windowPos = app.dom.$window.scrollTop() +  this._defineRatio(), // get the offset of the window from the top of page
				theID,
				$box,
				$boxPos,
				$boxHeight;

				//TODO: отрефакторить!!!
				for (var i=0; i < this.targetBoxes.length; i++) {
					theID = '#' + this.targetBoxes[i];
					$box = $(theID);

					if ($box.length) {
						$boxPos = Math.floor($box.offset().top);
						$boxHeight = Math.floor($box.height());


						 if (windowPos >= $boxPos && windowPos < $boxPos + $boxHeight) {
							 $("a[href='" + theID + "']").addClass("_is-active");

							 if (this.currentActiveLink !== theID)  {
								 this._activeLinkToCenter(this.targetBoxes[i]);
							 }
							 this.currentActiveLink = theID;
						 } else {
						 	$("a[href='" + theID + "']").removeClass("_is-active");
						 }
					}
				}
		},

		/**
		 * @description Центрирует активный линк относительно блока (если есть скролл)
		 * @private
		 */
		_activeLinkToCenter: function(targetLink) {
			var $activeLink = this.$('[data-target="'+ targetLink +'"]'),
				position = $activeLink.is(':first-child') ? 0 : Math.abs(this.scrollBox.scrollLeft() + $activeLink.offset().left - (this.scrollBox.width() / 2 - $activeLink.width() / 2));

				this.scrollBox.animate({
					scrollLeft : position
				}, 300);
		},

		/**
		 * @description Анимирует переход к целевому блоку
		 * @param e
		 * @private
		 */
		_goTo: function(e) {
			e.preventDefault();

			var thisView = e.data.self;

			var $this = $(this),
				$targetBox = $('#' + $this.data('target')),
				$targetPos = Math.ceil($targetBox.offset().top - thisView._defineRatio());

				thisView._animateToTargetBox($targetPos);
		},

		/**
		 * description Управляет перемещениями между блоками по нажатию на кнопки next/prev
		 * @param e
		 * @private
		 */
		_goToNextPrev: function(e) {
			e.preventDefault();

			var thisView = e.data.self,
				$this = $(this),
				$links = $('.js-sticky-link'),
				direction = $this.data('dir'),
				curActiveItem = $links.filter('._is-active'),
				$targetBox = direction === 'next' ? curActiveItem.next() : curActiveItem.prev();

				if (curActiveItem.length) {
					if ($targetBox.length) {
						$targetBox = $('#' + $targetBox.data('target'));
					}
				}else {
					$targetBox = $('#' + $($links[0]).data('target'));
				}

				if ($targetBox.length) {
					thisView._animateToTargetBox(Math.ceil($targetBox.offset().top - thisView._defineRatio()));
				}
		},

		/**
		 * @description Анимирует переход к нужному блоку
		 * @param $targetPos
		 * @private
		 */
		_animateToTargetBox: function($targetPos) {
			$("html, body").animate({ scrollTop: $targetPos }, 300);
		},

		initialize: function() {
			this.header = $('#mainHeader');
			this.scrollBox = $('#scrollBox');
			this.relativeBox = $('#mainInfo');
			this.detailPageHeader = $('#detailPageHeader');

			this.arrowLeft = this.$('.js-sticky-arrow-left');
			this.arrowRight = this.$('.js-sticky-arrow-right');

			this._defineMainData();
			this._defineTargetBoxes();

			this._setInitialStickyBoxPosY();
			this._setActiveClassToLink();
			this._stickyNavTrigger();

			if (app.layoutType === 'mobile') {
				this._initDetailHeaderController();
			}

			this._subscribers();
		},


		/**
		 * @description Переключает состояние прилипающей навигации на fixed, или обратно на absolut
		 * @private
		 */
		_stickyNavTrigger: function() {
			var curPosY = app.dom.$document.scrollTop(),
				headersHeight = this.headerHeight + this.detailPageHeaderHeight; // Высота фикседных блоков

			if (this.posY > curPosY + headersHeight) {
				this.$el.removeClass('_is-fixed').css('top', this.posY);

				this.isFixedState = false;
			}else {
				this.$el.addClass('_is-fixed').css('top', this.$el.css('top', headersHeight - 1 ));

				this.isFixedState = true;
			}
		},

		_initDetailHeaderController: function() {
			detailHeaderController.init();
		},
		_destroyDetailHeaderController: function() {
			detailHeaderController.destroy();
		},

		_subscribers: function() {
			app.dom.$window.on('scroll.stickyTrigger', this._stickyNavTrigger.bind(this));
			app.dom.$window.on('scroll.stickyActiveClass', this._setActiveClassToLink.bind(this));
			app.dom.$window.on('resize.detailMainData', this._defineMainData.bind(this));

			this.links.on('click', {self: this}, this._goTo);

			this.arrowLeft.on('click', {self: this},  this._goToNextPrev);
			this.arrowRight.on('click', {self: this}, this._goToNextPrev);

			Backbone.on('mobileLayout', this._initDetailHeaderController);
			Backbone.on('laptopLayout desktopLayout', this._destroyDetailHeaderController);

			Backbone.on('textInHeaderHide', this._defineMainData.bind(this));
			Backbone.on('textInHeaderShowed', this._defineMainData.bind(this));
		},

		render: function() {

		},

		destroy: function() {

		}
	});

});