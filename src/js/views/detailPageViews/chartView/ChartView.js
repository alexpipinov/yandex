define(function(require) {
	'use strict';

	var $ = require('jquery'),
		Backbone = require('backbone'),
		Chart = require('charts');

	return Backbone.View.extend({
		el: "#myChart",

		options: {
			responsive: true,
			animation: false,
			///Boolean - Whether grid lines are shown across the chart
			scaleShowGridLines : false,
			//String - Colour of the grid lines
			scaleGridLineColor : "rgba(0,0,0,.05)",
			//Number - Width of the grid lines
			scaleGridLineWidth : 1,
			//Boolean - Whether the line is curved between points
			bezierCurve : true,
			//Number - Tension of the bezier curve between points
			bezierCurveTension : 0.4,
			//Boolean - Whether to show a dot for each point
			pointDot : true,
			//Number - Radius of each point dot in pixels
			pointDotRadius : 4.5,
			//Number - Pixel width of point dot stroke
			pointDotStrokeWidth : 2,
			//Number - amount extra to add to the radius to cater for hit detection outside the drawn point
			pointHitDetectionRadius : 20,
			//Boolean - Whether to show a stroke for datasets
			datasetStroke : true,
			//Number - Pixel width of dataset stroke
			datasetStrokeWidth : 2,
			//Boolean - Whether to fill the dataset with a colour
			datasetFill : true,
			// String - Tooltip background colour
			tooltipTitleFontFamily: "'gotham, arial', 'Helvetica', 'Arial', sans-serif",
			tooltipFillColor: "rgba(255,255,255,1)",
			tooltipFontSize: 12,
			tooltipFontColor: "#000",
			// String - Tooltip title font weight style
			tooltipTitleFontStyle: "bold",
			tooltipTemplate: "<%= value %> оценок",
			// Number - pixel width of padding around tooltip text
			tooltipYPadding: 10,
			// Number - pixel width of padding around tooltip text
			tooltipXPadding: 20,
			tooltipCaretSize: 10
		},

		initialize: function() {
			this._subscribers();

			this._initChart();
			this.render();
		},

		_subscribers: function() {

		},

		_getChartData: function() {
			var data = {
				labels: ["янв", "фев", "март", "апр", "май", "июнь", "июль", "авг", "сент", "окт"],
				datasets: [
					{
						label: "Estimates",
						fillColor: "rgba(179,179,179, .2)",
						strokeColor: "rgba(179,179,179,.2)",
						pointColor: "rgba(179,179,179, 1)",
						pointStrokeColor: "#b3b3b3",
						pointHighlightFill: "#fff",
						pointHighlightStroke: "rgba(0, 165, 80,1)",
						data: [1350, 4148, 1640, 3419, 2286, 427, 890, 1034, 3482, 4603]
					}
				]
			};
			return data;
		},

		_initChart: function() {
			var ctx = document.getElementById("myChart").getContext("2d");
			this.chartInstance = new Chart(ctx).Line(this._getChartData(), this.options);
		},

		render: function() {
			return this;
		},

		destroy: function() {
			this.chartInstance.destroy();
		}
	});
});