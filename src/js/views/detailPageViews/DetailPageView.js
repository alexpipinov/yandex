define(function(require) {
	'use strict';
	var $ = require('jquery'),
		Baсkbone = require('backbone'),
		app = require('app');

	//Views
	var MapView = require('views/detailPageViews/mapView/MapView'),
		StickyNavView = require('views/detailPageViews/stickyNavView/StickyNavView'),
		CarouselView = require('views/detailPageViews/carousel/CarouselView');

	var goTo = require('modules/common/goTo');

	return Backbone.View.extend({
		el: '#detailPage',

		initialize: function() {
			this._initStickyNav();
			this.carouselInstance = new CarouselView();

			//Инициализируем модель для плавной прокрутки к нужным позициям
			goTo.init();

			if (app.layoutType !== 'mobile') {
				this._initChart();
			}

			this._initMap();

			this._subscribers();
			this.render();
		},

		_subscribers: function() {
		},

		_initChart: function() {
			var self = this;

			require(['views/detailPageViews/chartView/ChartView'], function(ChartView) {
				self.chartInstance = new ChartView();
			});
		},

		_destroyChart: function() {
			this.chartInstance.destroy();
			this.chartInstance = null;
		},

		_initStickyNav : function() {
			this.stickyNavInstance = new StickyNavView();
			this.stickyNavInstance = null;
		},
		_destroyStickyNav : function() {
			this.stickyNavInstance.destroy();
			this.stickyNavInstance = null;
		},

		_initMap: function() {
			this.map = new MapView();
		},

		render: function() {
			return this;
		},

		_destroyMap: function(){
			this.map.destroy().remove();
		},

		destroy: function() {
			this._destroyMap();
			this._destroyStickyNav();
			this.carouselInstance.destroy();

			this.chartInstance.destroy();
			goTo.destroy();
		}
	});
});
