define(function(require) {
	'use strict';

	var $ = require('jquery'),
		Backbone = require('backbone'),
		owlCarousel = require('owl');


	return Backbone.View.extend({
		el: '#detailCarousel',

		initialize: function() {
			this.carouselInstance = this.$el.owlCarousel({
				slideSpeed: 400,
				paginationSpeed: 400,
				rewindSpeed: 400,
				loop: true,
				singleItem: true,
				pagination: true,
				navigation: true,
				navigationText: ['<i class="b-carousel__arrow b-carousel__arrow_direction_left"></i>', '<i class="b-carousel__arrow b-carousel__arrow_direction_right"></i>'],
				mouseDrag: false
			});
			this.render();
		},

		render: function() {
			return this;
		},

		destroy: function() {
			this.carouselInstance.destroy();
			this.$carousel.data('owlCarousel').destroy();
		}
	});
});