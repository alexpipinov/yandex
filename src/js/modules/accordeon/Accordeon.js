/**
 * Модуль отвечает за открытие любых скрытых блоков( по типу аккордеона), без привязки к структуре документа
 */

(function(factory) {
	if (typeof define === 'function' && define.amd) {
		define(['jquery'], factory);
	}else {
		factory();
	}
})(function($) {

	//TODO: добавить опции
	// 1. отскролить в начало области просмотра,
	// 2. скрывать другие отрытые аккордеоны,
	// 3. тип плагина: аккордеон (тогда есть контейнер общий, получающий класс) или свобдный блок, открывающий какой-то блок)
	var Accordion = function($el) {
		if (!(this instanceof Accordion)) {
			return new Accordion($el);
		}

		this.options = {
			scrollToTop: true,
			singleItem: false,
			accordionType: true
		};

		this.$accordion = $el;
		this.timeout = null;

		this.subscribers = function() {
			this.$accordion.on('click', '.js-accordeon-header', this.stateTrigger.bind(this));
		};

		this.initialize = function() {
			this.subscribers();
		};
		this.destroy = function() {
			this.$accordion.off();
		};

		this.initialize();
	};

	Accordion.prototype = {
		/**
		 * @description Получает высоту скрытого блока
		 * @param $targetBox
		 * @returns {*}
		 */
		getContentBoxHeight : function($targetBox) {
			var height;
				//Временно задаем 'auto', чтобы можно было получить высоту
				$targetBox.css('height', 'auto');
				height 	= $targetBox.height();
				//Возвращаем обратно высоту в 0
				$targetBox.css('height', 0);

				return height;
		},

		/**
		 * Принимает решение скрыть или показать блок
		 * @param e
		 */
		stateTrigger : function(e) {
			e.preventDefault();

			this.$link = $(e.currentTarget);
			this.$targetBox = $('#' + this.$link.data('target'));

			if (this.options.accordionType) {
				this.$parent = this.$link.closest('.js-accordeon-item');
			}

			if (this.$link.hasClass('_is-active') || this.$parent.hasClass('_is-active')) {
				this.closeBox();
			}else {
				this.openBox();
			}
		},

		/**
		 * Открывает скрытый блок
		 */
		openBox : function() {
			this.$targetBox.css({'min-height': this.getContentBoxHeight(this.$targetBox)});

			if (this.options.accordionType) {
				this.$parent.addClass('_is-active');
			}else {
				this.$link.addClass('_is-active');
			}

			if (this.timeout) {
				clearTimeout(this.timeout);
			}
			this.timeout = setTimeout(this.scrollToTop.bind(this), 400);
		},

		/**
		 * Закрывает блок
		 */
		closeBox : function() {
			if (this.options.accordionType) {
				this.$parent.removeClass('_is-active');
			}else {
				this.$link.removeClass('_is-active');
			}

			this.$targetBox.css({'min-height': 0});
		},

		/**
		 * Отскроливает в начало области просмотра
		 */
		scrollToTop : function() {
			$('html, body').animate({
				scrollTop : this.$link.offset().top - $('#mainHeader').height()
			}, 400);
		}
	};


	return Accordion;
});