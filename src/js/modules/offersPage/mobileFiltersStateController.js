define(function(require) {
	'use strict';
	var $ = require('jquery'),
		Backbone = require('backbone'),
		app = require('app');

	var mobileFiltersStateController =   function() {
		var $openBtn,
			$closeBtn,

			initialize =  function() {
				$openBtn = $('.js-filters-mobile-btn');
				$closeBtn = $('.js-category-filters-close-btn');
				_subscribers();
			},

			/**
			 * @description Открывает панель фильтров
			 * @private
			 */
			_openFiltersBox = function() {
				app.dom.$body.addClass('i-lap-mob-filters-is-opened');
			},

			/**
			 * @description Закрывает панель фильтров
			 * @private
			 */
			_closeFiltersBox = function() {
				app.dom.$body.removeClass('i-lap-mob-filters-is-opened');
			},

			/**
			 * @description Триггерит событие openFiltersIsClicked для медиатора navSearchMediator
			 * @param e
			 * @private
			 */
			_openFiltersTrigger = function() {
				Backbone.trigger('openFiltersIsClicked', {name: 'openFilters'});
			},

			/**
			 * @description Триггерит событие closeFiltersIsClicked для медиатора navSearchMediator
			 * @param e
			 * @private
			 */
			_closeFiltersTrigger = function() {
				Backbone.trigger('closeFiltersIsClicked', {name: 'closeFilters'});
			},

			_subscribers = function() {
				$openBtn.on('click', _openFiltersTrigger);
				$closeBtn.on('click', _closeFiltersTrigger);

				Backbone.on('mobileFiltersOpen', _openFiltersBox);
				Backbone.on('mobileFiltersClose', _closeFiltersBox);
			},

			destroy = function() {
				$openBtn.off();
				$closeBtn.off();
				Backbone.off(null, null, this);
			};

			return {
				init: initialize,
				destroy: destroy
			}
	};

	return mobileFiltersStateController();
});