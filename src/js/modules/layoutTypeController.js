define(function(require) {
	'use strict';
	var $ = require('jquery'),
		enquire = require('enquire'),
		Backbone = require('backbone'),
		app	= require('app');

		//Регистрируем обработчики
		enquire.register("screen and (min-width: 1023px)", {
			match: function() {
				app.layoutType = 'desktop';
				Backbone.trigger('desktopLayout', {name: 'desktop'});
			},

			unmatch: function() {
				app.layoutType = 'laptop';
				Backbone.trigger('laptopLayout', {name: 'laptop'});
			}
		});

		enquire.register("screen and (max-width: 767px)", {
			match: function() {
				app.layoutType = 'mobile';
				Backbone.trigger('mobileLayout', {name: 'mobile'});
			},
			unmatch: function() {
				app.layoutType = 'laptop';
				Backbone.trigger('laptopLayout', {name: 'laptop'});
			}
		});
});