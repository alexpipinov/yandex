define(function(require) {
	var $ = require('jquery'),
		Backbone = require('backbone'),
		app = require('app');

	var detailHeaderController = function() {
		var $mainHeaderHeight = $('#mainHeader').height(),
			$detailHeader =  $('#detailPageHeader'),
			state = false, // запущен контроллер или нет

			/**
			 * @description Скрывает или показывает подписи к иконкам
			 * @private
			 */
			_textVisibilityTrigger =  function() {
				var scrollPosition = app.dom.$document.scrollTop();

				if (scrollPosition > $mainHeaderHeight) {
					$detailHeader.addClass('_hide-text');

					Backbone.trigger('textInHeaderHide');
				}else {
					$detailHeader.removeClass('_hide-text');
					Backbone.trigger('textInHeaderShowed');
				}
			},

			_subscribers =  function() {
				app.dom.$window.on('scroll.detailPageHeader', _textVisibilityTrigger);
			},

			init =  function() {
				if (!this.state) {
					state = true;
					_subscribers();
				}
			},

			destroy =  function() {
				if (state) {
					app.dom.$window.off('scroll.detailPageHeader');
					state = false;
				}
			};

			return {
				'init' : init,
				'destroy' : destroy
			}
	};

	return detailHeaderController();
});