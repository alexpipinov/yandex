define(function(require) {
	'use strict';

	var $ = require('jquery'),
		Backbone = require('backbone');

	return function() {
		var $btn = $('.js-favorite-btn'),

			/**
			 * @description Проверяет предложение - (в избранных / не в избранныех)
			 * @private
			 */
			_checkStatus = function(e) {
				e.stopPropagation();
				e.preventDefault();

				var $this = $(this);

				if ($this.hasClass('_is-active')) {
					_removeFromFavorites($this);
				}else {
					_addToFavorites($this);
				}
			},

			/**
			 * @description Добавляет предложение в избранные
			 * @private
			 */
			_addToFavorites = function(offer) {
				offer.addClass('_is-active');
			},

			/**
			 * @description Убирает предложение из избранных
			 * @private
			 */
			_removeFromFavorites = function(offer) {
				offer.removeClass('_is-active');
			},

			subscribers = function() {
				$btn.on('click', _checkStatus);
			},

			initialize = function() {
				subscribers();
			},

			destroy = function() {
				$btn.off('click');
			};

			return initialize();
	};
});