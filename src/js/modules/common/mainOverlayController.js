define(function(require) {
	'use strict';

	var $ = require('jquery'),
		Backbone = require('backbone');

	var mainOverlayController = function() {
		var $overlay = $('#mainOverlay'),
			overlayState = $overlay.hasClass('_is-active') ? true : false,

			_stateTrigger = function() {
				if (overlayState) {
					$overlay.removeClass('_is-active');

					Backbone.trigger('mainOverlayIsHidden');
					$overlay.off('click.mainOverlay');
					overlayState = false;
				}else {
					$overlay.addClass('_is-active');

					$overlay.on('click.mainOverlay', _stateTrigger);
					overlayState = true;
				}
			},

			_subscribers = function() {
				Backbone.on('showMainOverlay', _stateTrigger);
			},

			initialize = function() {
				_subscribers();
			};

		return initialize();
	};

	return mainOverlayController;
});