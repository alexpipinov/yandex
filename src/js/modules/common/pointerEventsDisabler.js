define(function(require) {
	'use strict';

	var body 	= document.body,
		timer,

		_initEvents = function() {
			window.addEventListener('scroll', function() {
				clearTimeout(timer);
				if(!body.classList.contains('i-disable-hover')) {
					body.classList.add('i-disable-hover')
				}

				timer = setTimeout(function(){
					body.classList.remove('i-disable-hover')
				}, 200);
			}, false);
		},

		initialize = function() {
			_initEvents();
		};

		return initialize;
});