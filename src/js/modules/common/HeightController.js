define(function(require) {
	'use strict';

	var $ = require('jquery');

	var HeightController = function(box, desktopRatio, mobileRatio) {
		this.box = box;
		this.ratio = desktopRatio;
		this.mobileRatio = mobileRatio || false;

		this.initialize();
	};

	HeightController.prototype  = {
		setHeight : function() {
			this.box.height(this.getBoxWidth() * this.ratio);
		},

		getBoxWidth: function() {
			return this.box.width();
		},
		initEvents : function() {
			$(window).on('resize.HeightController', this.setHeight.bind(this));
		},
		initialize: function() {
			this.setHeight();
			this.initEvents();
		},
		destroy: function() {
			$(window).off('resize.HeightController');
		}
	};

	return HeightController;
});