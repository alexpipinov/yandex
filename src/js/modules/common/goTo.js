define(function(require) {
	'use strict';
	var $ = require('jquery'),
		app = require('app');

	var goTo = function() {
		var $goToBtn,

			_goToBlock = function(e) {
				e.preventDefault();

				var $this 		= $(this),
					$target		= $this.data('target') == 0 ? 0 : $('#' + $this.data('target')),
					$targetPos 	= $target != 0 ? $target.offset().top - _defineRatio() : 0;

				$("html, body").animate({
					scrollTop: $targetPos
				}, 600);
			},

			_defineRatio = function() {
				if ($('body').hasClass('i-detail-page')) {
					return app.layoutType === 'mobileLayout' ? 120 : 150;
				}else {
					return 0;
				}
			},

			_subscribers = function() {
				$goToBtn.on('click', _goToBlock);
			},


			initialize = function() {
				$goToBtn = $('.js-go-to');
				_subscribers();
			},

			destroy = function() {
				$goToBtn.off();
			};


			return {
				init : initialize,
				destroy: destroy
			};
	};

	return goTo();
});
