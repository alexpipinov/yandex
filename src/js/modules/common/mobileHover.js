define(function(require) {
	'use strict';

	var $ = require('jquery');

	var mobileHover = function() {
		var $hoverBoxes,

			_stateTrigger = function(e) {
				e.stopPropagation();

				var $this = $(this);
				console.log($this);

				if (!$this.hasClass('_mobile-hover')) {
					e.preventDefault();

					setTimeout(function() {
						$hoverBoxes.removeClass('_mobile-hover');
						$this.addClass('_mobile-hover');
					}, 100);

				}
			},
			_initEvents = function() {
				$hoverBoxes.on('touchend',  _stateTrigger);
			},

			initialize = function() {
				$hoverBoxes = $('.js-mobile-hover');
				_initEvents();
			};

			return initialize();
	};

	return mobileHover;
});