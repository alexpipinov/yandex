define(function() {
	'use strict';

	return {
		getScrollX : function() {
			return window.pageXOffset || document.documentElement.scrollLeft;
		},

		getScrollY : function() {
			return window.pageYOffset || document.documentElement.scrollTop;
		},

		isTouchDevice: function() {
			return 'ontouchstart' in document.documentElement;
		},

		isWebkit    : function() {
			return 'WebkitAppearance' in document.documentElement.style;
		},


		getHeightContainer : function($targetBox) {
			var $contentHeight;

			$targetBox.css('height', 'auto'); 				                	// Для анимирования высоты контейнера со значение 'auto' сначала быстро проверим его высоту
			$contentHeight 	= $targetBox.outerHeight(); 		            	// высота целевого блока

			$targetBox.css('height', '0');					                	// Вернём обратно значение в '0'

			return $contentHeight;
		}
	};
});