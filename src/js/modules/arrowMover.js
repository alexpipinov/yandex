define(function(require) {
	'use strict';

	var $ = require('jquery'),
		app = require('app');

	var arrowMover = function(targetSlide) {
		var $arrow = $('.js-arrow-top'),
			relativeBoxes = $('.js-carousel-controll'),
			activeBox,

			/**
			 * Получает позицию активного элемента
			 * @returns {*}
			 * @private
			 */
			_getBoxPosition = function() {
				return activeBox.offset().left;
			},

			/**
			 * Получает ширину активного элеента
			 * @returns {*}
			 * @private
			 */
			_getBoxWidth = function() {
				return activeBox.width();
			},

			/**
			 * Выставляем стрелке позицию относительно активного контролла
			 * @private
			 */
			_setArrowPosition = function() {
				var posX = _getBoxPosition() + (_getBoxWidth() / 2);

				$arrow.css({
					"-webkit-transform":"translate(" + posX + "px, 0)",
					"-ms-transform":"translate(" + posX + "px, 0)",
					"transform":"translate(" + posX + "px, 0)"
				});
			},

			/**
			 * Показать стрелку для контролла (при первой загрузке отрабатывает)
			 * @private
			 */
			_showArrow = function() {
				$arrow.addClass('is-visible');
			},

			initialize = function() {
				if (!targetSlide) {
					//При первой загрузке определяем активный контролл по классу
					activeBox = relativeBoxes.filter('.is-active');

					_setArrowPosition();

					//Таймаут специально, чтобы успел transition отработать первый, а потом уже показать стрелку
					setTimeout(function() {
						_showArrow();
					}, 400);
				}else {
					//При переездах карусели узнаем следующий активный контролл из номера слайда
					activeBox = relativeBoxes.filter('[data-item='+ targetSlide +']');
					_setArrowPosition();
				}
			};

			initialize();
	};

	return arrowMover;
});