define(function(require) {
	'use strict';

	var $ = require('jquery'),
		_ = require('underscore'),
		Backbone = require('backbone');

	return function() {
		var $link = $('.js-popup-content-trigger'),
			params,
			$curBox,

			/**
			 * Открывает нужный блок
			 * @private
			 */
			_openTargetBox = function() {
				$curBox.addClass('_is-active');
			},

			/**
			 * Закрывает текущий блок
			 * @private
			 */
			_closeCurrentBox = function() {
				$curBox.removeClass('_is-active');
				$curBox = null;
			},

			/**
			 * Определяет целевой блок и вызывает обработчик состояний
			 * @param e
			 * @param callback
			 * @private
			 */
			_getParams = function(e) {
				e.preventDefault();
				params = $(this).data();

				if (params.content) {
					_stateTrigger();
				}
			},

			_stateTrigger = function() {
				if ($curBox) {
					_closeCurrentBox();
				}else {
					$curBox = $(params.content);
					if ($curBox) {
						//Если требуется оверлей
						if (params.overlay) {
							Backbone.trigger('showMainOverlay');
						}
						_openTargetBox();
					}
				}
			},

			subscribers = function() {
				$link.on('click.popupContent', _getParams);
				Backbone.on('mainOverlayIsHidden', _stateTrigger);
			},

			initialize = function() {
				subscribers();
			},

			destroy = function() {
				$link.off('click.popupContent');
			};

		return initialize();
	}
});