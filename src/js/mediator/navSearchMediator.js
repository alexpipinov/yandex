define(function(require) {
	'use strict';

	var $ = require('jquery'),
		_ = require('underscore'),
		app = require('app'),
		Backbone = require('backbone');

	var NavSearchMediator = function () {
			//Храним состояния навигации и поисковых блоков
			this.navState = false;
			this.desktopSearchState = false;
			this.mobileFiltersState = false;
			this.contentOverlayState = false;

			/**
			 * Проверяем откуда пришло событие и вызываем нужный обработчик
			 * @param event
			 */
			this.checkTypeAction = function(event) {
				var target = event.name;
					switch (target) {
						case 'navBtn':
							this.navBtnHandler();
							break;
						case 'headerSearchBtn':
							this.headerSearchBtnHandler();
							break;
						case 'contentOverlay':
							this.contentOverlayHandler();
							break;
						case 'desktopSearchClose':
							this.headerSearchBtnHandler();
							break;
						case 'openFilters':
							this.filtersStateHandler();
							break;
						case 'closeFilters':
							this.filtersStateHandler();
							break;
						default :
							break;
					}
			};

			//Обработчик состояний для панели фильтров(моб/лап версии)
			this.filtersStateHandler = function() {
				var self = this;

				if (this.mobileFiltersState) {
					this.mobileFiltersClose();
				}else {
					this.mobileFiltersOpen();
				}

			};

			//Обработчик для кнопки навигации
			this.navBtnHandler = function() {
				var self = this;

				if (this.navState) {
					this.navClose();
				}else {
					//Если открыт поисковый блок, то скрываем сначала его
					if (this.desktopSearchState) {
						this.desktopSearchClose();

						//Даем анимации закрыти поискового блока отработать
						setTimeout(function() {
							self.navOpen();
						}, app.animationSpeeds.long);
					}else if (this.mobileFiltersState) { // Если открыты фильтры мобильные, то сначала их скрываем
						this.mobileFiltersClose();

						//Даем анимации закрыти поискового блока отработать
						setTimeout(function() {
							self.navOpen();
						}, app.animationSpeeds.long);
					}else {
						self.navOpen();
					}
				}
			};

			//Обработчик для кнопки поиска в шапке
			this.headerSearchBtnHandler = function() {
				var self = this;

				if (this.desktopSearchState) {
					this.desktopSearchClose();
				}else {
					//Если открыто меню, скрываем
					if (this.navState) {
						this.navClose();

						//Даем анимации закрытия отработать
						setTimeout(function() {
							self.desktopSearchOpen();
						}, app.animationSpeeds.long);
					}else {
						self.desktopSearchOpen();
					}
				}
			};

			//Обработчик клика по основному оверлею
			this.contentOverlayHandler = function() {
				if (this.contentOverlayState) {
					if (this.navState) {
						this.navBtnHandler();
					}
					if (this.desktopSearchState) {
						this.headerSearchBtnHandler();
					}
					if (this.mobileFiltersState) {
						this.filtersStateHandler();
					}
				}
			};


			//Открывает навигацию
			this.navOpen = function() {
				Backbone.trigger('navOpen');
				this.navState = true;
				this.contentOverlayState = true;
			};
			//Закрывает навигацию
			this.navClose = function() {
				Backbone.trigger('navClose');
				this.navState = false;
				this.contentOverlayState = false;
			};
			//Открывает поисковый блок (десктоп)
			this.desktopSearchOpen = function() {
				Backbone.trigger('desktopSearchOpen');
				this.desktopSearchState = true;
				this.contentOverlayState = true;
			};
			//Закрывает поисковый блок (десктоп)
			this.desktopSearchClose = function() {
				Backbone.trigger('desktopSearchClose');
				this.desktopSearchState = false;
				this.contentOverlayState = false;
			};
			//Открывает мобильные фильтры
			this.mobileFiltersOpen = function() {
				Backbone.trigger('mobileFiltersOpen');
				this.mobileFiltersState = true;
				this.contentOverlayState = true;
			};

			//Закрывает мобильные фильтры
			this.mobileFiltersClose = function() {
				Backbone.trigger('mobileFiltersClose');
				this.mobileFiltersState = false;
				this.contentOverlayState = false;
			};


			this.initialize = function() {
				this.subscribers();
			};

			this.subscribers = function() {
				Backbone.on('headerSearchBtnClicked', this.checkTypeAction, this);
				Backbone.on('navBtnIsClicked', this.checkTypeAction, this);
				Backbone.on('contentOverlayIsClicked', this.checkTypeAction, this);
				Backbone.on('desktopSearchCloseBtnIsClicked', this.checkTypeAction, this);
				Backbone.on('openFiltersIsClicked', this.checkTypeAction, this);
				Backbone.on('closeFiltersIsClicked', this.checkTypeAction, this);
			};

			this.destroy = function() {

			};

			this.initialize();
	};

	_.extend(NavSearchMediator, Backbone.Events);

	return NavSearchMediator;
});