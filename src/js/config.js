require.config({
	baseUrl				: 	'src/js/',

	deps				:	['main'],

	paths: {
		jquery			: 	'libs/jquery/dist/jquery.min',
		underscore		: 	'libs/underscore/underscore',
		backbone		: 	'libs/backbone/backbone',
		modernizr		: 	'libs/modernizr/modernizr',
		ddslick			: 	'libs/ddslick/ddslick',
		enquire			: 	'libs/enquire/enquire.min',
		cookies			: 	'libs/cookies/cookie',
		fastclick		: 	'libs/fastclick/lib/fastclick',
		async			: 	'libs/requirejs-plugins/src/async',
		scrollbar		: 	'libs/scrollbar/scrollbar.min',
		charts			: 	'libs/chart/Chart.min',
		twig			:	'libs/twig/twig.min',
		ymaps			:	'//api-maps.yandex.ru/2.1/?lang=ru_RU',
		owl				:	'libs/owl/owl',
		iCheck			:	'libs/icheck/icheck',
		selecter		:	'libs/selecter/selecter',
		helpers			:	'modules/helpers/helpers',
		pointerEvents	: 	'modules/common/pointerEventsDisabler'
	},
	shim: {
		'owl' : {
			deps : ['jquery'],
			exports : '$'
		},
		"iCheck": {
			deps: ["jquery"],
			exports: "jQuery.fn.iCheck"
		},
		"ymaps": {
			exports: "ymaps"
		}
	},
	waitSeconds			:	0
});


