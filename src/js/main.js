require([
	// Load our app module and pass it to our definition function
	'jquery',
	'app',
	'modernizr',
	'router',
	'helpers',
	'pointerEvents'
], function($, app, modernizr, Router, helpers, pointerEventsDisabler){

	$(function() {
		app.router = new Router();

		// задание '/' в качестве корневого каталога по умолчанию.
		// можно изменить его в app.js.
		Backbone.history.start({ pushState: true, root: app.root });

		if (!helpers.isTouchDevice()) {
			pointerEventsDisabler();
		}

		//Если у ссылки есть атрибут `data-bypass`, то полностью исключаем делегирование.
		/*app.dom.$document.on("click", "a[href]:not([data-bypass])", function(evt) {
			// Получение абсолютного href якоря.
			var href = {
				prop: $(this).prop("href"),
				attr: $(this).attr("href")
			};

			// Получение абсолютного корня.
			var root = location.protocol + "//" + location.host + app.root;


			// Корень должен входить в href якоря, указывая на его относительность.
			if (href.prop.slice(0, root.length) === root) {
				evt.preventDefault();
				evt.stopPropagation();

				Backbone.history.navigate(href.attr, true);
			}
		});*/
	});

});