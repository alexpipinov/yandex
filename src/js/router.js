// Filename: router.js
define(function(require) {
	'use strict';

	var Backbone = require('backbone'),
		app	= require('app'),
		Fastclick = require('fastclick');


	//Global views
	var HeaderView = require('views/globalViews/HeaderView'),
		MenuView = require('views/globalViews/MenuView'),
		ContentOverlayView = require('views/globalViews/ContentOverlayView'),
		SearchBoxView = require('views/searchBox/SearchBoxView');

	//Modules
	var layoutTypeController = require('modules/layoutTypeController'),
		NavSearchMediator = require('mediators/navSearchMediator'),
		popupContentMediator = require('mediators/popupContentMediator'),

		mainOverlayController = require('modules/common/mainOverlayController');

	return Backbone.Router.extend({
		routes: {
			''					: 	'home',
			'offerList'			: 	'offer',
			'detailOffer'		: 	'detailOffer',
			'notifications'		: 	'notifications',
			'payHistory'		:	'payHistory',
			'earn'				:	'earn',
			'*other'			: 	'notFound'
		},

		home: function() {
			var self = this;

			require(['views/mainPageViews/mainPageView'], function(MainPageView) {
				self.viewController(MainPageView);
			});
		},

		offer: function() {
			var self = this;

			require(["views/offers/OffersMainView"], function (OffersMainView) {
				self.viewController(OffersMainView);
			});
		},

		detailOffer: function() {
			var self = this;

			require(["views/detailPageViews/DetailPageView"], function (DetailPageView) {
				self.viewController(DetailPageView);
			});
		},

		payHistory: function() {
			var self = this;


			require(["views/payHistoryViews/PayHistoryView"], function (PayHistoryView) {
				self.viewController(PayHistoryView);
			});
		},

		earn: function() {
			var self = this;

			require(["views/earnViews/EarnView"], function (EarnView) {
				self.viewController(EarnView);
			});
		},

		notifications: function() {
			var self = this;

			require(["views/notificationsViews/NotificationsView"], function (NotificationsView) {
				self.viewController(NotificationsView);
			});
		},

		notFound: function() {
			app.cleanCurrentViews();
		},

		initialize : function() {
			//Инициализируем библиотеку fastclick для обработки click-ов на мобильных устройствах
			Fastclick.attach(document.body);

			// Global views
			app.globalViews.push(new HeaderView());
			app.globalViews.push(new MenuView());
			app.globalViews.push(new ContentOverlayView());
			app.globalViews.push(new SearchBoxView());

			//NavSearchBehavior mediator
			app.navSearchMediator = new NavSearchMediator();
			app.popupContentMediator = popupContentMediator();

			//Инициализируем глобальный модуль основного оверлея
			mainOverlayController();
		},


		viewController: function(View) {
			if (app.loadCounter) {
				app.currentViews.push(new View());
				app.loadCounter = false;
			}else {
				//Чистим от предыдущих вьюх
				app.cleanCurrentViews();

				app.currentViews.push(new View());
			}
		}
	});
});