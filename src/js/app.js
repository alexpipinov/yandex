define(function(require) {
	'use strict';

	var Backbone 	= require('backbone'),
		_ 			= require('underscore'),
		helpers		= require('helpers');

	var app = {
		root : '/',

		//По умолчанию тип шаблона - laptop; далее матчим mediaqueries для определения типа шаблона
		layoutType: 'laptop',

		//Содержит views общие для всех страниц
		globalViews: [],

		//Содержит views актуальные для текущего состояния
		currentViews: [],

		animationSpeeds: {
			short : '200',
			long : '400'
		},

		dom : {
			$window 	: $(window),
			$document 	: $(document),
			$body		: $('body')
		},

		navSearchMediator: {},


		loadCounter: true, // true: страница грузится впервый; false: страница грузится по ajax

		/**
		 * Проверяет массив с текущими views и очищает его
		 */
		cleanCurrentViews: function() {
			if (this.currentViews.length) {
				//Проходимся по массиву app.currentViews и очищаем
				_.invoke(app.currentViews, 'remove');
				app.currentViews.length = 0;
			}
		}
	};

	return _.extend(app, Backbone.Events);
});