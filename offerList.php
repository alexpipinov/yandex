<?php

require_once 'lib/Twig/Autoloader.php';
Twig_Autoloader::register();

$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader, array(
		'cache'       => 'compilation_cache',
		'auto_reload' => true,
		'debug'		  => true
));
$twig->addExtension(new Twig_Extension_Debug());

//Тестовые данные для шаблонов
$categoryList = array(
		array(
				'id' => '1',
				'name'=>'Поесть и выпить',
				'color' => 'black',
				'preview'=> array(
						'big' => 'upload/category/1.jpg',
						'small' => 'upload/category/small/1.jpg'
				),
				'deataillUrl'=>'offerList.php'
		),
		array(
				'id' => '2',
				'name'=>'Красота и здоровье',
				'color'=>'white',
				'preview'=> array(
						'big' => 'upload/category/2.jpg',
						'small' => 'upload/category/small/2.jpg'
				),
				'deataillUrl'=>'offerList.php'
		),
		array(
				'id' => '3',
				'name'=>'Отдохнуть и развлечься',
				'color'=>'black',
				'preview'=> array(
						'big' => 'upload/category/3.jpg',
						'small' => 'upload/category/small/3.jpg'
				),
				'deataillUrl'=>'offerList.php'
		),
		array(
				'id' => '4',
				'name'=>'Товары для детей',
				'color'=>'white',
				'preview'=> array(
						'big' => 'upload/category/1.jpg',
						'small' => 'upload/category/small/4.jpg'
				),
				'deataillUrl'=>'offerList.php'
		),
		array(
				'id' => '5',
				'name'=>'Стиль и шоппинг',
				'color'=>'white',
				'preview'=> array(
						'big' => 'upload/category/1.jpg',
						'small' => 'upload/category/small/5.jpg'
				),
				'deataillUrl'=>'offerList.php'
		),
		array(
				'id' => '6',
				'name'=>'Авто-мото',
				'color'=>'white',
				'preview'=> array(
						'big' => 'upload/category/1.jpg',
						'small' => 'upload/category/small/6.jpg'
				),
				'deataillUrl'=>'offerList.php'
		),
		array(
				'id' => '7',
				'name'=>'Путешествия, поездки',
				'color'=>'white',
				'preview'=> array(
						'big' => 'upload/category/1.jpg',
						'small' => 'upload/category/small/7.jpg'
				),
				'deataillUrl'=>'offerList.php'
		),
		array(
				'id' => '8',
				'name'=>'Фото-видео',
				'color'=>'white',
				'preview'=> array(
						'big' => 'upload/category/1.jpg',
						'small' => 'upload/category/small/8.jpg'
				),
				'deataillUrl'=>'offerList.php'
		),
		array(
				'id' => '9',
				'name'=>'Продукты, все для дома',
				'color'=>'white',
				'preview'=> array(
						'big' => 'upload/category/1.jpg',
						'small' => 'upload/category/small/9.jpg'
				),
				'deataillUrl'=>'offerList.php'
		),
		array(
				'id' => '10',
				'name'=>'Помощь в быту',
				'color'=>'white',
				'preview'=> array(
						'big' => 'upload/category/1.jpg',
						'small' => 'upload/category/small/10.jpg'
				),
				'deataillUrl'=>'offerList.php'
		)
);

$categoryFilters = array(
	array(
			'groupName' => 'Итак, вам нужно место для:',
			'type' => 'radio',
			'hasCustomFilters' => array(),
			'filters' => array(
				array('filterName' => 'Завтрак перед работой', 'displayType' => 'block', 'groupName' => 'case'),
				array('filterName' => 'Обеда с коллегами', 'displayType' => 'block', 'groupName' => 'case'),
				array('filterName' => 'Деловой встречи вне офиса', 'displayType' => 'block', 'groupName' => 'case'),
				array('filterName' => 'Ужина c любимым человеком', 'displayType' => 'block', 'groupName' => 'case'),
				array('filterName' => 'Доставка еды домой', 'displayType' => 'block', 'groupName' => 'case'),
			)
	),
	array(
			'groupName' => 'Со средним чеком:',
			'type' => 'radio',
			'hasCustomFilters' => array(),
			'filters' => array(
					array('filterName' => 'Любым', 'displayType' => 'block', 'groupName' => 'check'),
					array('filterName' => '$', 'displayType' => 'inline', 'groupName' => 'check'),
					array('filterName' => '$$', 'displayType' => 'inline', 'groupName' => 'check'),
					array('filterName' => '$$$', 'displayType' => 'inline', 'groupName' => 'check'),
					array('filterName' => '$$$$', 'displayType' => 'inline', 'groupName' => 'check'),
			)
	),
	array(
			'groupName' => 'Недалеко от:',
			'type' => 'radio',
			'hasCustomFilters' => array('type' => 'plus'),
			'filters' => array(
					array('filterName' => 'Меня', 'displayType' => 'two-clmn', 'groupName' => 'location'),
					array('filterName' => 'Работы', 'displayType' => 'two-clmn', 'groupName' => 'location'),
					array('filterName' => 'Дома', 'displayType' => 'two-clmn', 'groupName' => 'location'),
					array('filterName' => 'Метро...', 'displayType' => 'two-clmn', 'groupName' => 'location'),
			)
	),
	array(
			'groupName' => 'Открытое:',
			'type' => 'radio',
			'hasCustomFilters' => array('type' => 'clock'),
			'filters' => array(
					array('filterName' => 'Сейчас', 'displayType' => 'two-clmn', 'groupName' => 'schedule'),
					array('filterName' => 'С 9 утра', 'displayType' => 'two-clmn', 'groupName' => 'schedule'),
					array('filterName' => '24 часа', 'displayType' => 'two-clmn', 'groupName' => 'schedule'),
					array('filterName' => 'после 23', 'displayType' => 'two-clmn', 'groupName' => 'schedule'),
			)
	),
	array(
			'groupName' => '',
			'type' => 'checkbox',
			'hasCustomFilters' => array(),
			'filters' => array(
					array('filterName' => 'Только где есть скидки и акции', 'displayType' => 'block', 'groupName' => 'activeSales'),
			)
	),
	array(
			'groupName' => 'С кухней',
			'type' => 'custom',
			'hasCustomFilters' => array('type' => 'plus'),
			'filters' => array()
	),
	array(
			'groupName' => 'Предлагающее',
			'type' => 'custom',
			'hasCustomFilters' => array('type' => 'plus'),
			'filters' => array()
	),
	array(
			'groupName' => 'Тип заведения',
			'type' => 'custom',
			'hasCustomFilters' => array('type' => 'plus'),
			'filters' => array()
	)
);

$offers = array(
	array(
		'name' => 'Коллекция с Эшли Бенсон',
		'description' => 'Средний чек 1500 -  2000 р., оплата картой, WIFI, средний чек 1500 -  2000 р., оплата картой, WIFI, живая музыка.',
		'detailUrl' => 'detailOffer.php',
		'preview' => 'upload/cheapest/1.jpg',
		'subway' => 'м. Курская',
		'address' => 'Большая Дмитровка, дом 32, стр.1',
		'location' => '55.760591, 37.579672',
		'phone' => '+7 (495) 456-45-45',
		'cheapest' => true,
		'schedule' => array(
			array(
				'days'=> 'пн-вт',
				'time'=> '12:00 - 18:00'
			),
			array(
				'days'=> 'сб-вс',
				'time'=> '10:00 - 18:00'
			)
		),
		'rating' => array(
			'average' => '4.7',
			'totalVotes' => '23'
		),
		'displayType' => 'dark',
		'sale' => array(
			'percentage' => '33',
			'conditions' => 'Скидка 33% на первое посещение или на 3 сеанса массажа',
			'residues' => '2'
		)
	),
	array(
			'name' => 'Кафе Cakehouse',
			'description' => 'Средний чек 1500 - 2000 р., оплата картой, WIFI, средний чек 1500 -  2000 р., оплата картой, WIFI, живая музыка.',
			'detailUrl' => 'detailOffer.php',
			'preview' => 'upload/cheapest/2.jpg',
			'subway' => 'м. Волгоградский проспект',
			'address' => 'Большая Дмитровка, дом 32, стр.1',
			'location' => '55.760591, 37.579672',
			'phone' => '+7 (495) 456-45-45',
			'cheapest' => true,
			'schedule' => array(
					array(
							'days'=> 'пн-вт',
							'time'=> '12:00 - 18:00'
					)
			),
			'rating' => array(
					'average' => '4.7',
					'totalVotes' => '23'
			),
			'displayType' => 'white',
			'sale' => array()
	),
	array(
			'name' => 'Fresh бар',
			'description' => 'Средний чек 1500 - 2000 р., оплата картой, WIFI, средний чек 1500 -  2000 р., оплата картой, WIFI, живая музыка.',
			'detailUrl' => 'detailOffer.php',
			'preview' => 'upload/cheapest/3.jpg',
			'subway' => 'м. Волгоградский проспект',
			'address' => 'Большая Дмитровка, дом 32, стр.1',
			'location' => '55.760591, 37.579672',
			'phone' => '+7 (495) 456-45-45',
			'cheapest' => false,
			'schedule' => array(
					array(
							'days'=> 'пн-вт',
							'time'=> '12:00 - 18:00'
					)
			),
			'rating' => array(
				'average' => '3.4',
				'totalVotes' => '93'
			),
			'displayType' => 'white',
			'sale' => array()
	),
	array(
			'name' => 'China wall',
			'description' => 'Средний чек 1500 - 2000 р., оплата картой, WIFI, средний чек 1500 -  2000 р., оплата картой, WIFI, живая музыка.',
			'detailUrl' => 'detailOffer.php',
			'preview' => 'upload/cheapest/4.jpg',
			'subway' => 'м. Волгоградский проспект',
			'address' => 'Большая Дмитровка, дом 32, стр.1',
			'location' => '55.760591, 37.579672',
			'phone' => '+7 (495) 456-45-45',
			'cheapest' => false,
			'schedule' => array(
					array(
							'days'=> 'пн-вт',
							'time'=> '12:00 - 18:00'
					)
			),
			'rating' => array(
					'average' => '3.4',
					'totalVotes' => '93'
			),
			'displayType' => 'white',
			'sale' => array()
	),
	array(
			'name' => 'Русская кухня',
			'description' => 'Средний чек 1500 - 2000 р., оплата картой, WIFI, средний чек 1500 -  2000 р., оплата картой, WIFI, живая музыка.',
			'detailUrl' => 'detailOffer.php',
			'preview' => 'upload/cheapest/5.jpg',
			'subway' => 'м. Волгоградский проспект',
			'address' => 'Большая Дмитровка, дом 32, стр.1',
			'location' => '55.760591, 37.579672',
			'phone' => '+7 (495) 456-45-45',
			'cheapest' => true,
			'schedule' => array(
					array(
							'days'=> 'пн-вт',
							'time'=> '12:00 - 18:00'
					)
			),
			'rating' => array(
					'average' => '3.4',
					'totalVotes' => '93'
			),
			'displayType' => 'white',
			'sale' => array()
	),
	array(
			'name' => 'Коллекция с Эшли Бенсон',
			'description' => 'Средний чек 1500 - 2000 р., оплата картой, WIFI, средний чек 1500 -  2000 р., оплата картой, WIFI, живая музыка.',
			'detailUrl' => 'detailOffer.php',
			'preview' => 'upload/cheapest/1.jpg',
			'subway' => 'м. Волгоградский проспект',
			'address' => 'Большая Дмитровка, дом 32, стр.1',
			'location' => '55.760591, 37.579672',
			'phone' => '+7 (495) 456-45-45',
			'cheapest' => true,
			'schedule' => array(
					array(
							'days'=> 'пн-вт',
							'time'=> '12:00 - 18:00'
					)
			),
			'rating' => array(
					'average' => '3.4',
					'totalVotes' => '93'
			),
			'displayType' => 'white',
			'sale' => array()
	)

);

//Конец тестовых данных для шаблона

echo $twig->render('pages/offers-page/offers-page.twig',
		array(
			'categoryFilters' => $categoryFilters,
			'categoryList' => $categoryList,
			'offers' => $offers
		)
);