module.exports = function(grunt) {

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		less: {
			build : {
				options: {
					paths	: ["src/less"],
					cleancss: true
				},
				files: {
					"src/less/<%= pkg.name %>.concat.css": "src/less/styles.less"
				}
			}
		},


		autoprefixer: {
			options: {
				browsers: ['last 2 version', '> 1%', 'Explorer 9']
			},
			dist: {
				files: {
					"build/<%= pkg.name %>.min.css" : "src/less/<%= pkg.name %>.concat.css"
				}
			}

		},

		requirejs: {
			compile: {
				options: {
					baseUrl: "src/js",
					mainConfigFile: "js/config.js",
					include: ['config'], // Include the main module defined
					wrap: true, // Wrap everything up in a closure
					generateSourceMaps: true, // Experimental
					preserveLicenseComments: false, // Needs turned off for generateSourceMaps
					optimize: "uglify2", // Supports generateSourceMaps
					out: "build/<%= pkg.name %>.js"
				}
			}
		},


		watch: {
			css: {
				files: ['src/less/**/*.less'],
				tasks: ['css']
			}
		}

	});


	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.loadNpmTasks('grunt-contrib-requirejs');
	grunt.loadNpmTasks('grunt-contrib-watch');



	grunt.registerTask('css', ['less', 'autoprefixer']);
	grunt.registerTask('jsBuild', ['requirejs']);
	grunt.registerTask('default'    , ['watch']);
};